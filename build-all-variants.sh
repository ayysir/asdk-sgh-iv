#!/bin/bash
export KERNELDIR=/home/ayysir/android/kernel/asdk-sgh-iv

# Colorize and add text parameters
red=$(tput setaf 1) # red
grn=$(tput setaf 2) # green
cya=$(tput setaf 6) # cyan
txtbld=$(tput bold) # Bold
bldred=${txtbld}$(tput setaf 1) # red
bldgrn=${txtbld}$(tput setaf 2) # green
bldblu=${txtbld}$(tput setaf 4) # blue
bldcya=${txtbld}$(tput setaf 6) # cyan
txtrst=$(tput sgr0) # Reset

echo -e "${bldred} Make Clean ${txtrst}"
make clean

echo ""

echo -e "${bldred} Make Mrproper ${txtrst}"
make mrproper

echo ""

echo -e "${bldred} Compile TMO Variant ${txtrst}"
./ASDK-TMO-GS4.sh

echo ""
echo ""

echo -e "${bldred} Make Clean ${txtrst}"
make clean

echo ""

echo -e "${bldred} Make Mrproper ${txtrst}"
make mrproper

echo ""

echo -e "${bldred} Compile ATT Variant ${txtrst}"
./ASDK-ATT-GS4.sh


echo ""

echo "Finished all Builds!"

exit 0

