#!/bin/bash
#These are definitions to enable shortcoding commands which makes the script look cleaner
#Check out the folder at the end of the directory paths, make your own but keep files in these directories

DEFCONFIG="make VARIANT_DEFCONFIG=jf_tmo_defconfig SELINUX_DEFCONFIG=jfselinux_defconfig asdk_jf_defconfig"
THREADS="`grep 'processor' /proc/cpuinfo | wc -l`"
export KERNELDIR=/home/ayysir/android/kernel/asdk-sgh-iv
export INITRAMFS_DEST=$KERNELDIR/kernel/usr/initramfs
export PACKAGEDIR=/home/ayysir/android/kernel/kernelwork/asdk_gs4_out
export INITRAMFS_SOURCE=/home/ayysir/android/kernel/Ramdisks
export Meta=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/META-INF
export Drivers=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/Drivers
export PREP=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/PREP
export extras=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/updated_modules
export System=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/system
export Data=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/data
export USE_SEC_FIPS_MODE=true
export USE_CCACHE=1
export CCACHE_DIR=/home/ayysir/.ccache
export ARCH=arm
export CROSS_COMPILE=/home/ayysir/android/Roms/beanstalk/prebuilts/gcc/linux-x86/arm/arm-eabi-4.6/bin/arm-eabi-
# export CROSS_COMPILE=/home/ayysir/android/kernel/toolchains/codefirex_4.7/bin/arm-eabi-

# Colorize and add text parameters
red=$(tput setaf 1) # red
grn=$(tput setaf 2) # green
cya=$(tput setaf 6) # cyan
txtbld=$(tput bold) # Bold
bldred=${txtbld}$(tput setaf 1) # red
bldgrn=${txtbld}$(tput setaf 2) # green
bldblu=${txtbld}$(tput setaf 4) # blue
bldcya=${txtbld}$(tput setaf 6) # cyan
txtrst=$(tput sgr0) # Reset

echo -e "${bldred} Set CCACHE ${txtrst}"
ccache -M50

echo ""

echo -e "${bldcya} Switch to JellyBean AOSP 4.2${txtrst}"
cd $KERNELDIR 
git checkout cm-10.1

echo ""

echo -e "${bldred} Remove old Package Files ${txtrst}"
rm -rf $PACKAGEDIR/*

echo ""

echo -e "${bldred} Setup Out Directories ${txtrst}"
mkdir -p $PACKAGEDIR/system/lib/modules

echo ""

echo -e "${bldred} Remove old zImage ${txtrst}"
rm arch/arm/boot/zImage

echo ""

echo -e "${bldred} Remove old Ramdisk ${txtrst}"
rm $INITRAMFS_SOURCE/ramdisk.gs4.gz

echo ""

echo -e "${bldred} Removing pesky backup files ${txtrst}"
cd ~/android/kernel
find ./ -name '*~' | xargs rm

echo ""

echo -e "${bldred} Make Clean ${txtrst}"
cd $KERNELDIR
make clean

echo ""

echo -e "${bldred} Use Defconfig Settings ${txtrst}"
script -q ~/Compile.log -c "
$DEFCONFIG

make -j$THREADS" 

echo ""

if [ -e $KERNELDIR/arch/arm/boot/zImage ]; then
	echo -e "${bldred} Copy modules to Package ${txtrst}"
	cp -a $(find . -name *.ko -print |grep -v initramfs) $PACKAGEDIR/system/lib/modules/
 	
	echo ""

	echo -e "${bldred} Copy zImage to Package ${txtrst}"
	cp $KERNELDIR/arch/arm/boot/zImage $PACKAGEDIR/zImage
 	
	echo ""

	echo -e "${bldred} Ramdisk Readying.. ${txtrst}"
	cp $KERNELDIR/mkbootfs $INITRAMFS_SOURCE
	cd $INITRAMFS_SOURCE/
	./mkbootfs ASDK-GS4 | gzip > ramdisk.gs4.gz
	rm mkbootfs
 	
	echo ""

	echo -e "${bldred} Making Boot.img.. ${txtrst}"
	cp $KERNELDIR/mkbootimg $PACKAGEDIR
	cp $INITRAMFS_SOURCE/ramdisk.gs4.gz $PACKAGEDIR
	cd $PACKAGEDIR
	./mkbootimg --cmdline 'console = null androidboot.hardware=qcom user_debug=31 zcache' --kernel $PACKAGEDIR/zImage --ramdisk $PACKAGEDIR/ramdisk.gs4.gz --base 0x80200000 --pagesize 2048 --ramdisk_offset 0x02000000 --output $PACKAGEDIR/boot.img
	rm mkbootimg
 	
	echo ""

	echo -e "${bldred} Extra Important files ${txtrst}"	
	cp -R $Meta $PACKAGEDIR
	cp -R $System/* $PACKAGEDIR/system
	cp $extras/* $PACKAGEDIR/system/lib/modules
	cp $Drivers/* $PACKAGEDIR/system/lib/
 	cp -R $PREP $PACKAGEDIR

	echo ""

	export curdate=`date "+%m-%d-%Y"`
        cp ~/Compile.log ~/android/compile_logs/Success-GS4-$curdate.log
        rm ~/Compile.log
	cd /home/ayysir/android/kernel/Ramdisks
	rm ramdisk.gs4.gz
	cd $PACKAGEDIR
	rm ramdisk.gs4.gz
	rm zImage
	rm ../../ASDK-GS4-3.4.x-*.zip
	zip -r ../ASDK-GS4-3.4.x-$curdate-Alpha.zip .
	mv ../ASDK-GS4-3.4.x-$curdate-Alpha.zip ~/android/kernel
	cp ~/android/kernel/ASDK-GS4-3.4.x-$curdate-Alpha.zip  /home/ayysir/Dropbox

	echo "ASDK HOT OFF THE PRESS"

else
	echo "KERNEL DID NOT BUILD! no zImage exist"
	export curdate=`date "+%m-%d-%Y"`
	cp ~/Compile.log ~/android/compile_logs/Failed-GS4-$curdate.log
fi;

exit 0

